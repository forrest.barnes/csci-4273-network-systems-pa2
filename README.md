# CSCI 4273 Network Systems PA2

This is a multi-threaded web server written in Rust. It supports GET, POST, and keepalive functionality.

## Running the code

1. Install [Rust](https://www.rust-lang.org/tools/install)
2. ``git clone https://gitlab.com/forrest.barnes/csci-4273-network-systems-pa2.git``
3. ``cd csci-4273-network-systems-pa2``
4. ``cargo run --release``
    - The default port number is 8888
    - ``cargo run --release -- 8080`` sets the port number to 8080
5. Test the code

## Testing

To verify multi-threaded functionality:

1. Open a web browser tab at ``localhost:8888/sleep``. This will take 5 seconds to load.
2. Open a new tab at ``localhost:8888``. This will load instantly, even if the ``sleep`` tab is loading.

It is theoretically possible to open any number of tabs at a time this way, but one will run into limitations depending on the OS/hardware the code is run on.

To show that the server joins all started threads, run the code without the ``--release`` flag. The debug messages printed out in this configuration show the number of threads still running after the ``thread_joiner`` function joins all finished threads. Some of these threads will take 11 seconds to finish due to the keepalive functionality, but after 11 seconds of inactivity the number of threads should always go to 0.

Other tests should be run using ``netcat`` rather than ``telnet``, since ``telnet`` does not appear to wait long enough for the server to respond.